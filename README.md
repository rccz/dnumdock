# Django Nginx Uwsgi and MySql Docker Containers
This repo contains the configuration for Django application development docker containers using MySql, Uwsgi and Nginx.

## Preinstallation
##### Clone the repo
```
git clone git@bitbucket.org:rccz/dnumdock.git
```
##### Change .env variable values as your new application needs
The repo is prepared to run the initial configuration, only the specific values must be changed:

* APP_NAME (The Django application name)
* PYTHON_PORT (the port used by uwsgi, default is 8001)
* WEB_PORT (the web swever port, default 80)
* MYSQL_PORT (Port number used by db server for db manager access. On future versions will be managed by PhpMyAdmin)
* MYSQL_ROOT_PASSWORD
* MYSQL_DATABASE (Database name, Default app_db)
* MYSQL_USER
* MYSQL_PASSWORD
* TZ (MySql Timezone)

##### Additional configuration changes:
Prepare the settings.py file with the following configuration, replacing database name, user, password with the ones you defined in .env file.
```
import pymysql

pymysql.install_as_MySQLdb()

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'APP_NAME',
        'USER': 'MYSQL_USER',
        'PASSWORD': 'MYSQL_PASSWORD',
        'HOST': 'db',
        'PORT': '3306',
    }
}


STATIC_URL = '/static/'
STATIC_ROOT = '/static'
MEDIA_URL = '/media/'
MEDIA_ROOT = '/media'
```
## Installation
##### Create Database & Application
Command for database and django project creation (replace APP_NAME with your application name)
```
docker-compose run python django-admin.py startproject APP_NAME .
```
###### *Permissions issues*
*If you can't edit files, run the following command:*
```
sudo chmod a+rwx -R staticroot/ && sudo chmod a+rwx -R src/ && sudo chmod a+rwx -R data/
```
##### Edit settings.py
Replace the configuration into settings.py file with the configurations edited in preinstallation step (replacing database name, user, password with the ones you defined in .env file.)

##### Database migrations
```
docker-compose run python ./manage.py migrate
```
##### Create django admin super user
```
docker-compose run python ./manage.py createsuperuser
```
##### Create static files repository
```
docker-compose run python ./manage.py collectstatic
```
##### Final Docker install
Finally create the docker containers and keep them alive
```
docker-compose up -d
```
###### Credits
Thanks to Ken Kono for his original article
> [Docker for Django(Nginx and MySQL)](https://medium.com/@kenkono/docker-for-django-nginx-and-mysql-5960a611829e)